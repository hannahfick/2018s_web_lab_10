package ictgradschool.web.lab10.exercise04;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HitCounterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if ("true".equalsIgnoreCase(req.getParameter("removeCookie"))) {

            //TODO - add code here to delete the 'hits' cookie

            for (Cookie co : req.getCookies()) {
                if (co.getName().equals("hits")) {
                    co.setMaxAge(0);
                }
                resp.addCookie(co);
            }


        } else {

            //TODO - add code here to get the value stored in the 'hits' cookie then increase it by 1 and update the cookie


            Cookie[] cookies = req.getCookies();
            boolean exists = false;

            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("hits")) {
                        int i = Integer.valueOf(cookie.getValue());
                        i++;
                        cookie.setValue("" + i);
                        resp.addCookie(cookie);
                        exists = true;
                    }
                }
                if (exists == false) {
                    Cookie c = new Cookie("hits", "1");
                    resp.addCookie(c);
                }


                //TODO - use the response object's send redirect method to refresh the page
            } else {
                Cookie c = new Cookie("hits", "1");
                resp.addCookie(c);
            }
        }

        resp.sendRedirect("hit-counter.html");

    }
}



