package ictgradschool.web.lab10.exercise01;

public class User {

    String userFname = null;
    String userLname = null;
    String userCountry = null;
    String userCity = null;

    public User(String userFname, String userLname, String userCountry, String userCity) {

        this.userFname = userFname;
        this.userLname = userLname;
        this.userCountry = userCountry;
        this.userCity = userCity;
    }

    public String getUserFname(){

        return userFname;
    }

    public String getUserLname(){

        return userLname;
    }

    public String getUserCountry(){

        return userCountry;
    }

    public String getUserCity(){

        return userCity;
    }


}
